// Load required packages.
var mongoose = require('mongoose');
const {Schema} = require("mongoose");

// Define the User schema.
var UserSchema = new mongoose.Schema({
    // "name" - String.
    // Users cannot be created (or updated) without a name or email.
    // All other fields that the user did not specify should be set to reasonable values.
    // Multiple users with the same email cannot exist.
    name: {type: String, required: true},
    // "email" - String.
    email: {type: String, required: true, unique: true},
    // "pendingTasks" - [String] - The _id fields of the pending tasks that this user has.
    // 每个task都会有一个_id, 是Mongoose自动分配的，pendingTasks就是这些task object的id的list。
    pendingTasks: [{
        type: Schema.Types.ObjectId, // 使用ObjectId来引用其他文档
        ref: 'Task' // 'Task' 是Task模型的名称
    }],
    // "dateCreated" - Date - should be set automatically by server.
    dateCreated: {type: Date, default: Date.now}
}, {
    // 通过设置versionKey: false，告诉Mongoose不要自动添加这个__v字段到你的数据库文档中。这样，当你查看数据库中的用户文档时，就不会看到__v字段。
    versionKey: false
});

// Export the Mongoose model.
module.exports = mongoose.model('User', UserSchema);

