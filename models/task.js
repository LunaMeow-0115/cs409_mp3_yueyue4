// Load required packages.
var mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define the Task schema.
var TaskSchema = new mongoose.Schema({
    // "name" - String.
    //Tasks cannot be created (or updated) without a name or a deadline.
    // All other fields that the user did not specify should be set to reasonable values.
    name: {type: String, required: true},
    // "deadline" - Date.
    deadline: {type: Date, required: true},
    // "description" - String.
    description: {type: String, default: ""},
    // "completed" - Boolean.
    completed: {type: Boolean, default: false},
    // "assignedUser" - String - The _id field of the user this task is assigned to - default "".
    // 每个user都会有一个_id, 是Mongoose自动分配的，代表该task是某个user分配的。
    assignedUser: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    // "assignedUserName" - String - The name field of the user this task is assigned to - default "unassigned".
    assignedUserName: {type: String, default: "unassigned"},
    // "dateCreated" - Date - should be set automatically by server to present date.
    dateCreated: {type:Date, default: Date.now}
}, {
    // 通过设置versionKey: false，告诉Mongoose不要自动添加这个__v字段到你的数据库文档中。这样，当你查看数据库中的用户文档时，就不会看到__v字段。
    versionKey: false
});

// Export the Mongoose model.
module.exports = mongoose.model('Task', TaskSchema);

