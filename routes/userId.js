// 定义User为User schema的模型。
var User = require('../models/user');
var Task = require('../models/task');
var ObjectId = require('mongoose').Types.ObjectId

module.exports = function (router) {

    // /users/:id 的路径。用于根据id查看某一个user的具体细节。
    var userRoute = router.route('/users/:id');

    // GET 请求。包括select query 。
    userRoute.get(async function (req, res) {
        try {

            // 从请求的URL中获取用户ID。
            const userId = req.params.id;
            // 创建查询。用Mongoose中的findById方法进行查询。返回一个满足ID和User model的对象。
            let query = User.findById(userId);

            // select 查询。
            if (req.query.select) {
                try {
                    // parse出select对应的JSON 。
                    const select = JSON.parse(req.query.select);
                    // select的选择的范围。
                    const validSelectFields = ['_id', 'name', 'email', 'pendingTasks', 'dateCreated'];
                    let selectConditions = {};
                    // 如果输入的内容在选择范围之内，或者数值是0和1就加入selectFields 。
                    for (let field in select) {
                        if (validSelectFields.includes(field) && [0, 1].includes(select[field])) {
                            selectConditions[field] = select[field];
                        }
                    }

                    // 如果一个有效field都没有被包含！没有的话就报错！
                    if (Object.keys(selectConditions).length === 0) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you used invalid parameters for select query. " +
                                "Notice, select query only takes '_id', 'name', 'email', 'pendingTasks', and 'dataCreated' as parameters, and the values can only be set to 0 or 1. " +
                                "Plus, you have to choose at least one field as the parameter. " +
                                "Additionally, '_id' will always be shown only if you straightforwardly set it to 0." +
                                "Note! When you set more than one filed in select query, except the '_id' filed, all other fields should be set to 0 or 1, you cannot set some of them to 1 and others to 0.",
                            data: []
                        });
                    }
                    // Mongoose 的 select 方法。
                    query = query.select(selectConditions);

                    //console.log("Query after select:", query);

                    // 捕获除了invalid fields 以外的任何错误！
                } catch (error) {
                    //console.log(error)
                    return res.status(400).json({
                        message: "400 Error -> Sorry, some error happened when parsing select query, please try again." +
                            "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                            "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception.",
                        data: []
                    });
                }
            }

            // 执行查询。注意,select只是不显示，不代表删除了这些元素。
            const user = await query.exec();
            // 检查用户是否存在。不存在的话返回404错误。
            if (!user) {
                return res.status(404).json({
                    message: "404 Not Found Error -> Sorry, this user has not been found.",
                    data: []
                });
            }
            // 如果找到了，返回。200成功。
            res.status(200).json({
                message: "200 Success -> This user  is retrieved found successfully!",
                data: user
            });

            // 所有错误处理。500错误。
        } catch (error) {
            res.status(500).json({
                message: "500 Error -> Sorry, cannot get the satisfied information! Please try again." +
                    "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                    "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception.",
                data: []
            });
        }
    });


    // PUT 请求。更新user的信息。根据PUT的规则，其实是替换整个内容。就是说，不能只写更新的内容上去，还是要提供更新之后的完整版内容。
    userRoute.put(async function (req, res) {
        try {
            // 取id 。取的是当前的，要被修改的user的id 。
            const original_userId = req.params.id;

            // ！为了迎合dataFill 的时候改成 let
            // let const { name, email, pendingTasks } = req.body;
            const { name, email, pendingTasks } = req.body;

            // 如果userData里不包括name或者email的话， 返回错误。
            if (!name || !email) {
                return res.status(400).json({
                    message:  "400 Error -> Sorry, both the name and the email of the user are required! Please try again.",
                    data: []
                });
            }

            // 检查 email 是否重复，除了当前用户之外！
            const existingEmail = await User.findOne({
                email: email,
                _id: { $ne: original_userId }
            });
            // 如果 email 重复，返回错误。
            if (existingEmail) {
                return res.status(400).json({
                    message: "400 Error -> Sorry, this user's email is already in use! Please try another email.",
                    data: []
                });
            }

            // update预备第一步。
            const userToUpdate = await User.findById(original_userId);
            if (!userToUpdate) {
                return res.status(404).json({
                    message: "404 Error -> The user you want to update has not been found.",
                    data: []
                });
            }

            // 更新已完成任务的 assignedUserName ！
            await Task.updateMany(
                { assignedUser: original_userId, completed: true },
                { $set: { assignedUserName: name }}
            );

            // 更新user的信息！
            userToUpdate.name = name;
            userToUpdate.email = email;
            userToUpdate.dateCreated = new Date();

            // 找出被删除的任务！！！userToUpdate.pendingTasks有的但是req.body里的pendingTasks里没有的！
            const tasksToRemove = userToUpdate.pendingTasks.filter(taskId =>
                !pendingTasks.includes(taskId.toString())
            );
            //console.log(tasksToRemove)

            // 解绑被删除的任务！代表已经完成！
            for (const taskId of tasksToRemove) {
                const task = await Task.findById(taskId);
                if (task) {
                    // assignedUser 的 id 是不变的！
                    task.assignedUserName = userToUpdate.name;
                    task.completed = true;
                    await task.save();
                }
            }

            /* !为了dataFill！允许pendingTasks=单个任务id ！
            userToUpdate.pendingTasks = [];
            if (typeof pendingTasks === 'string') {
                pendingTasks = [pendingTasks]; // 将单个任务 ID 转换为数组
            }*/

            // 更新剩余的 pendingTask！
            // 纯pendingTasks 就是req.body 里写的。
            // 把合格的更新到一开始为空的userToUpdate.pendingTasks 里。
            userToUpdate.pendingTasks = [];
            if (Array.isArray(pendingTasks)) {
                //console.log("yes")
                for (const taskId of pendingTasks) {
                    //console.log(taskId)
                    //console.log(typeof taskId)
                    if (!ObjectId.isValid(taskId)) {
                        //console.log("啦啦啦")
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you entered invalid type of inputs pendingTasks! The inputs must be the object id of task!",
                            data: []
                        });
                    }
                    // 找到 task 。
                    let task = await Task.findById(taskId);
                    //console.log("look")
                    //console.log(task)
                    // 检查是否存在。
                    if (!task) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you put an id of a task in pendingTasks which does not exist!",
                            data: []
                        });
                    }
                    // 检查task 是否被完成！
                    if (task.completed) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you put a task which is already completed in pendingTasks! " +
                                "You can only put not completed tasks!",
                            data: []
                        });
                    }
                    // 检查task 是否已经有assignedUser而且这个user还不是当前更新后的user 。original_userId也可以用userToUpdate._id。
                    if (task.assignedUser && task.assignedUser.toString() !== original_userId) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you put a task which already has an assignedUser! " +
                                "You can only put a task whose assignedUser is null!",
                            data: []
                        });
                    }

                    // 更新任务的分配状态！
                    task.assignedUser = userToUpdate._id;
                    task.assignedUserName = userToUpdate.name;
                    //console.log("here")
                    await task.save();
                    //console.log(task)
                    // 添加合格的task ！
                    if (!userToUpdate.pendingTasks.includes(task._id)) {
                        userToUpdate.pendingTasks.push(task._id);
                    }
                    //console.log(userToUpdate)
                }
            }

            //console.log("1")
            // 彻底保存userToUpdate!
            await userToUpdate.save();
            //console.log(userToUpdate)
            //console.log("2")
            // 如果成功找到，返回更新后的信息给客户端！200成功。
            res.status(200).json({
                message: "200 Success -> This user has been updated successfully!",
                data: userToUpdate
            });

            // 所有错误，500 错误。
        } catch (error) {
            //console.error("Error saving user:", error);
            res.status(500).json({
                message: "500 Server Error -> Sorry, cannot update this user!",
                data: []
            });
        }
    });


    // DELETE 请求。删除这个用户。
    userRoute.delete(async function (req, res) {
        try {
            // 取id 。
            const userId = req.params.id;
            // 找到要删除的user 信息。
            const userToDelete = await User.findById(userId);
            if (!userToDelete) {
                return res.status(404).json({
                    message: "404 Error -> Sorry, the user you wanna delete has not been found.",
                    data: []
                });
            }

            // 解绑所有 pendingTasks和这个user已经完成的任务!
            const tasksToUpdate = userToDelete.pendingTasks.concat(
                await Task.find({ assignedUser: userId, completed: true }).select('_id')
            );
            for (const task of tasksToUpdate) {
                await Task.findByIdAndUpdate(task, {
                    assignedUser: null,
                    assignedUserName: "unassigned",
                    // 因为completed=true的也放在里面了，所以要统一变成false
                    completed: false
                });
            }

            // 删除用户！
            await User.findByIdAndDelete(userId);
            res.status(200).json({
                message: "200 Success -> This user has been deleted successfully!",
                data: null
            });

            // 所有 error 。500错误。
        } catch (error) {
            res.status(500).json({
                message: "500 Server Error -> Sorry, some error happened when parsing select query, please try again." +
                    "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                    "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception",
                data: []
            });
        }
    });

    return router;
};

