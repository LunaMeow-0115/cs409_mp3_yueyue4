// Connect all endpoints together here.
// 这个模块导出一个函数，这个函数接受两个参数：app（一个 Express 应用实例）和 router（一个 Express 路由实例）。
module.exports = function (app, router) {

    // 为 /api 路径设置了一个路由处理程序，这个处理程序是从当前目录的 home.js 文件中导入的。它将 router 作为参数传递给 home.js。

    // app -> 一个 Express 应用实例，通常由 express() 函数创建。它是一个对象，提供了一系列方法用于配置服务器、添加中间件、定义路由等。

    // .use() ->  是一个用来挂载中间件函数到应用的方法。中间件函数是在请求执行过程中的一个或一系列函数，
    // 它们有机会访问请求和响应对象、执行代码、修改请求和响应对象、结束请求-响应循环、调用堆栈中的下一个中间件函数。

    // '/api' -> 一个路径前缀，表示 app.use() 中注册的中间件将应用于所有以 /api 开头的路径。
    // 它是一个路由级别的中间件，因此对应的处理函数仅对匹配该路径的请求有效。

    // require('./home.js') -> require 是一个 Node.js 的函数，用于导入模块。
    // 这里它正在导入当前目录下的 home.js 文件。该文件应该导出一个可以接受 router 参数的函数。

    // (router) -> router 是一个 Express 路由实例，通常由 express.Router() 创建。
    // 它是一个中间件和路由系统，可以被挂载到 Express 应用的路径上。这里，它被作为参数传递给 home.js 模块导出的函数。

    // require('./home.js')(router) -> 这个表达式结合了上面的 require 和 (router)。它首先使用 require 加载 home.js 模块，
    // 然后立即调用该模块导出的函数，并将 router 作为参数传递给它。结果是 home.js 模块返回一个配置好的 router 对象。

    /* 总结：
        app.use('/api', require('./home.js')(router)); 这行代码做了几件事情：
        1. require('./home.js') 导入了 home.js 文件。在 Node.js 中，require 是一个内置的函数，用于加载模块。
        2. home.js 中含有一个导出函数，这个函数接收一个router，并返回一个配置了路由的 router 对象。
        3. app.use 是一个 Express 方法，用于将中间件函数或路由挂载到应用的指定路径上，也就是 /api。
        4. 所以，require('./home.js')(router) 导入 home.js 模块，并立即调用它，传递 router 作为参数。
        5. 这个调用返回一个配置了路由的 router 对象，然后 app.use 把这个对象和 /api 路径关联起来。
        6. 这意味着任何以 /api 开始的 HTTP 请求都会通过这个 router 对象进行处理。
        home.js 中的路由器定义了一个 router.route('/')，那么这个路由实际上会响应 /api 的路径，因为 /api 是前缀。
        也就是说，/api 被添加到了/前面，整个路径为 http://localhost:4000/api/
    */

    app.use('/api', require('./home.js')(router));

    // /api/users 路径的处理程序来自于 users.js 文件。
    app.use('/api/users', require('./users.js')(router));
    // 为包含特定用户 ID 的路径（如 /api/users/XXXXXXXX）设置了一个路由处理程序，从 userId.js 文件中导入。
    app.use('/api/users/:id', require('./userId.js')(router));

    // 对于 /api/tasks 路径，使用从 tasks.js 文件导入的路由处理程序。
    app.use('/api/tasks', require('./tasks.js')(router));
    // 为包含特定任务 ID 的路径（如 /api/tasks/123）设置了一个路由处理程序，从 taskId.js 文件中导入。
    app.use('/api/tasks/:id', require('./taskId.js')(router));

};

