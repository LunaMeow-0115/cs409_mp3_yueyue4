var Task = require('../models/task');
var User = require('../models/user');

module.exports = function(router) {

    // GET 请求：根据 ID 获取特定任务。
    router.get('/tasks/:id', async (req, res) => {
        try {
            // 这个task/:id的帖子有问题的话，就返回错误。
            const taskId = req.params.id;

            // 处理 select 查询。
            let query = Task.findById(taskId);
            if (req.query.select) {
                try {
                    const select = JSON.parse(req.query.select);
                    const validSelectFields = ['_id', 'name', 'description', 'deadline', 'completed', 'assignedUser', 'assignedUserName', 'dateCreated'];
                    let selectConditions = {};

                    for (let field in select) {
                        if (validSelectFields.includes(field) && [0, 1].includes(select[field])) {
                            selectConditions[field] = select[field];
                        }
                    }

                    if (Object.keys(selectConditions).length === 0) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you entered invalid select parameters." +
                                "Notice, select query only takes '_id', 'name', 'deadline', 'completed', 'assignedUser', 'assignedUserName', and 'dateCreated' as parameters, and the values can only be set to 0 or 1. " +
                                "Plus, you have to choose at least one field as the parameter. " +
                                "Additionally, '_id' will always be shown only if you straightforwardly set it to 0." +
                                "Note! When you set more than one filed in select query, except the '_id' filed, all other fields should be set to 0 or 1, you cannot set some of them to 1 and others to 0.",
                            data: []
                        });
                    }

                    query = query.select(selectConditions);
                } catch (error) {
                    //console.log(error)
                    return res.status(400).json({
                        message: "400 Error -> Sorry, some error happened when parsing select query, please try again." +
                            "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                            "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception.",
                        data: []
                    });
                }
            }

            // 查询task存在不存在的时候根据query的返回结果来。
            const task = await query.exec();
            if (!task) {
                return res.status(404).json({
                    message: "404 Not Found Error -> Sorry, this task has not been found successfully.",
                    data: []
                });
            }

            // 成功找到。
            res.status(200).json({
                message:  "200 Success -> The task is retrieved successfully! Here is the list of tasks in database!",
                data: task
            });

        } catch (error) {
            res.status(500).json({
                message: "500 Error -> Sorry, cannot get the satisfied information! Please try again." +
                    "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                    "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception.",
                data: []
            });
        }
    });

    // PUT 请求：更新指定任务。
    router.put('/tasks/:id', async (req, res) => {
        try {
            // 取得任务ID和请求体中的数据。
            const taskId = req.params.id;
            const { name, deadline, completed, description, assignedUser } = req.body;

            // 确保任务名称和截止日期被提供。
            if (!name || !deadline) {
                return res.status(400).json({
                    message: "400 Error -> Sorry, both task name and deadline are required!",
                    data: []
                });
            }

            // 获取原始任务。
            const originalTask = await Task.findById(taskId);
            if (!originalTask) {
                return res.status(404).json({
                    message: "404 Not Found Error -> Sorry, this task has not been found.",
                    data: []
                });
            }

            // 处理assignedUser的更新。
            let updatedAssignedUser = null;
            if (assignedUser) {
                updatedAssignedUser = await User.findById(assignedUser);
                if (!updatedAssignedUser) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, the user you want to assign the task with does not exist.",
                        data: []
                    });
                }
            }

            // 更新任务。
            const updatedTaskData = {
                name,
                deadline,
                // 更新的时间。
                dateCreated: new Date(),
                description: description ? description : "",
                completed: completed !== undefined ? completed : originalTask.completed,
                assignedUser: updatedAssignedUser ? updatedAssignedUser._id : originalTask.assignedUser,
                assignedUserName: updatedAssignedUser ? updatedAssignedUser.name : originalTask.assignedUserName
            };

            // 完成更新！!
            const updatedTask = await Task.findByIdAndUpdate(taskId, updatedTaskData, { new: true });

            // 更新用户的pendingTasks。
            // 如果更新后completed = false 。
            if (!completed) {
                // 如果有req.body有写assignedUser而且合法 + originalTask 的 assignedUser != null。
                if (updatedAssignedUser && originalTask.assignedUser) {
                    // 如果更新的assignedUser 和原来的user id 一样。
                    if (originalTask.assignedUser.toString() === updatedAssignedUser._id.toString()) {
                        // originalUser 也就是 updatedUser 要增加这个新任务。
                        const originalUser = await User.findById(originalTask.assignedUser);
                        // 用include 判定，防止重复添加。
                        if (originalUser && !originalUser.pendingTasks.includes(updatedTask._id)) {
                            originalUser.pendingTasks.push(updatedTask._id);
                            await originalUser.save();
                        }
                        // 如果更新的assignedUser 和原来的user id 不一样。
                    } else {
                        // 从原用户的pendingTasks中移除任务。
                        const originalUser = await User.findById(originalTask.assignedUser);
                        if (originalUser && originalUser.pendingTasks.includes(updatedTask._id)) {
                            originalUser.pendingTasks.pull(taskId);
                            await originalUser.save();
                        }
                        // 并添加到新用户的pendingTasks中。
                        if (!updatedAssignedUser.pendingTasks.includes(updatedTask._id)){
                            updatedAssignedUser.pendingTasks.push(updatedTask._id);
                            await updatedAssignedUser.save();
                        }
                    }
                    // 如果有req.body有写assignedUser而且合法 + originalTask 的 assignedUser = null。
                } else if (updatedAssignedUser && !originalTask.assignedUser) {
                    // 更新task到updatedUser就好。
                    if (!updatedAssignedUser.pendingTasks.includes(updatedTask._id)){
                        updatedAssignedUser.pendingTasks.push(updatedTask._id);
                        await updatedAssignedUser.save();
                    }
                    // 如果有req.body没有写assignedUser + originalTask 的 assignedUser != null。
                } else if (!updatedAssignedUser && originalTask.assignedUser) {
                    // assignedUser保持原样，originalUser加入task。
                    const originalUser = await User.findById(originalTask.assignedUser);
                    if (originalUser && !originalUser.pendingTasks.includes(taskId)) {
                        originalUser.pendingTasks.push(taskId);
                        await originalUser.save();
                    }
                }
                // 如果没有req.body有写assignedUser + originalTask 的 assignedUser = null，什么都不用管。
            }
            // 如果更新后completed = true。
            // 如果originalTask.assigendUser = null ，没有updatedAssignedUser,不用管pendingTasks。
            // 如果originalTask.assigendUser = null ，有updatedAssignedUser, 不用管pendingTasks。
            // 如果originalTask.assigendUser != null ，没有updatedAssignedUser, originalUser的pendingTasks pull。
            // 如果originalTask.assigendUser != null ，有updatedAssignedUser而且和原来user id相同, originalUser也是updatedSAssignedUser的pendingTasks pull。
            // 如果originalTask.assigendUser != null ，有updatedAssignedUser但是不相同, originalUser也是updatedSAssignedUser的pendingTasks pull, updatedAssignedUser也不需要添加task因为已经完成了。
            if (completed) {
                if (originalTask.assignedUser) {
                    // 如果任务标记为已完成，从原用户的pendingTasks中移除。
                    const originalUser = await User.findById(originalTask.assignedUser);
                    if (originalUser && originalUser.pendingTasks.includes(taskId)) {
                        originalUser.pendingTasks.pull(taskId);
                        await originalUser.save();
                    }
                }
            }

            // 返回成功响应。
            res.status(200).json({
                message: "200 Success -> This task has been updated successfully!" +
                    "A kind reminder: In PUT task request, if this task was assigned to a valid user before, it will be ineffective if you put 'assignedUser' to null or an empty string.  " +
                    "Only if you delete the 'assignedUser', this task will be reset to unassigned. " +
                    "But if this task's 'assignedUser' is null, it is okay that you put null or an empty string to 'assignedUser', which will still keep this task unassigned.",
                data: updatedTask
            });

        } catch (error) {
            res.status(500).json({
                message: "500 Server Error -> Sorry, something wrong happened! You may check your whether your 'assignedUser' is legal. " +
                    "A kind reminder: In PUT task request, if this task was assigned to a valid user before, it will be ineffective if you put 'assignedUser' to null or an empty string.  " +
                    "Only if you delete the 'assignedUser', this task will be reset to unassigned. " +
                    "But if this task's 'assignedUser' is null, it is okay that you put null or an empty string to 'assignedUser', which will still keep this task unassigned.",
                data: []
            });
        }
    });


    // DELETE 请求：删除当前task ， 也引发一系列关于user 的操作。
    router.delete('/tasks/:id', async (req, res) => {
        try {
            const taskId = req.params.id;

            // 首先找到这个task，检查是否存在。
            const task = await Task.findById(taskId);
            if (!task) {
                return res.status(404).json({
                    message: "404 Not Found Error -> Task not found.",
                    data: []
                });
            }

            // 如果这个任务有assignedUser，那么需要从这个user的pendingTasks中删除这个task的id.
            if (task.assignedUser) {
                const assignedUser = await User.findById(task.assignedUser);
                if (assignedUser) {
                    assignedUser.pendingTasks.pull(taskId);
                    await assignedUser.save();
                }
            }

            // 删除任务！
            await Task.findByIdAndDelete(taskId);

            res.status(200).json({
                message: "200 Success -> Task deleted successfully",
                data: []
            });

        } catch (error) {
            res.status(500).json({
                message: "500 Server Error -> Sorry, something wrong happened!",
                data: []
            });
        }
    });

    return router;
};

