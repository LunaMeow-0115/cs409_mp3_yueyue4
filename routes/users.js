// 定义User为User schema的模型。
var User = require('../models/user.js');
var Task = require('../models/task');
// Object ID check!
var ObjectId = require('mongoose').Types.ObjectId

module.exports = function (router) {

    // /users 路径。
    var userRoute = router.route('/users');

    // GET请求，返回所有用户的列表。
    // 支持 where, sort, select,
    // async function 。
    userRoute.get(async function (req, res) {

        try {

            // req.query 是 localhost:4000/api/users?XXXX={...} 中，XXXX的部分。
            // 约定俗成有 ? 和 = 和 &, 约定俗成 {...} 是json 。
            //console.log("Received GET request on /users with query:", req.query);
            // Mongoose的find,查找符合User model的json
            let query = User.find();
            //console.log("query:", query);

            // 1. where 查询。
            // where可以支持所有 User fields 。 但是不能乱输入其他没有的 field 。
            // where 如果没有任何valid field ， 包括空，这种情况下就报错 。
            // req.query.where里的 where 是说如果 XXXX 中有关键词 "where"，如果是/users/where2={...}，那就是查找 if req.query.where2 。
            if (req.query.where) {
                try {
                    //console.log("req.query.where", req.query.where);
                    // parse where={...} 中括号里的json部分。这个部分的取变量名叫 where (const where里的where)。
                    const where = JSON.parse(req.query.where);
                    //console.log("const where = ", where);

                    // 定义有效fields 。
                    const validWhereFields = ['_id', 'name', 'email', 'dateCreated', 'pendingTasks'];
                    let whereConditions = {};
                    // where查找的必须是有效的fields 。
                    for (let field in where) {
                        if (validWhereFields.includes(field)) {
                            whereConditions[field] = where[field];
                        }
                    }
                    // whereConditions 如果是空的，也就是说如果没有任何一个field valid，就返回错误！
                    if (Object.keys(whereConditions).length === 0) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you used invalid or null parameters for where query. " +
                                "Notice，where query only takes '_id', 'name', 'email', 'dateCreated', and 'pendingTasks' as parameters. " +
                                "Plus, you should choose at least one field as the parameter.",
                            data: []
                        });
                    }

                    // query.where()里的where是 Mongoose 的 where的方法。帮助匹配查找 const where (也就是 json)的内容。
                    query = query.where(where);
                    //console.log("query", query);

                    // 捕获除了invalid fields 以外的其他任何的错误！
                } catch (error) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, some error happened when parsing where query, please try again.",
                        data: []
                    });
                }
            }

            // 2. sort 查询。
            // sort 只支持除了 pendingTasks的其他三个User fields 。 但是不能乱输入其他没有的 field 。
            // sort 如果是空。Mongoose自动返回所有记录。
            // XXXX中是否有"sort"关键词。
            if (req.query.sort) {
                try {
                    // 试图 parse sort={JSON} 的 JSON 。
                    const sort = JSON.parse(req.query.sort);
                    // 只允许 sort name, email, dateCreated !
                    const validSortFields = ['_id', 'name', 'email', 'dateCreated'];
                    // 记录有效的sort fields 。
                    const sortConditions = {};
                    // 对于用户想要sort的每一个filed，
                    for (let field in sort) {
                        // 如果 field valid , 如果数值是 1或者-1，就添加进去。
                        if (validSortFields.includes(field) && [1, -1].includes(sort[field])) {
                            sortConditions[field] = sort[field];
                        }
                    }
                    // 如果一个有效的sort field 都没有被包含，返回错误。
                    if (Object.keys(sortConditions).length === 0) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you used invalid parameters for sort query. " +
                                "Notice, sort query only takes '_id', 'name', 'email', and 'dateCreated' as parameters, and the values can only be set to 1 or -1. " +
                                "You can set sort query as null, in this way, you will get the original list of users without any sorting.",
                            data: []
                        });
                    }

                    // Mongoose 的 sort 方法进行排序。先满足 先出现的 field 的顺序。
                    // Mongoose 已经设定好 1 是 ascending , -1 是 descending 。
                    query = query.sort(sort);

                    // 捕获除了invalid fields 外的任何错误！
                } catch (error) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, some error happened when parsing sort query, please try again.",
                        data: []
                    });
                }
            }

            //console.log("Query before select:", query);


            // 3. select 查询。
            // select 支持所有有效 fields, 至少有一个 field。
            // 默认情况下，"_id"字段总是会出现，除非显式地标记它为0。
            // 如果只写了一个field ， 且为0，那么只有这个field不会被显示。
            // 如果只写了一个field , 且为1， 那么只会显示这个field。
            // 如果写了多于一个field , 且都为0，那么被写上的所有fields 都不会被显示。如果所有的fields都被写上去了而且都为0，那么就是显示空data 。
            // 如果写了多于一个field , 而且都为1，那么只显示被写上去的所有fields 。如果所有fields 都被写上取且都为1 ， 那么显示全部的data 。
            // 如果select里写多个fields ，那么他们必须都为1或者都为0，不能有1也有0。
            // 不过，在任何情况下，你都可以设置"_id"为0或者1！不管有多少个fields 。也就是说，除了默认的"_id"以外，其他的fields的0和1都要保持一致。
            // 如果query里有"select"关键词。
            if (req.query.select) {
                try {
                    // parse出select对应的JSON 。
                    const select = JSON.parse(req.query.select);
                    // select的选择的范围。
                    const validSelectFields = ['_id', 'name', 'email', 'pendingTasks', 'dateCreated'];
                    let selectConditions = {};
                    // 如果输入的内容在选择范围之内，或者数值是0和1就加入selectFields 。
                    for (let field in select) {
                        if (validSelectFields.includes(field) && [0, 1].includes(select[field])) {
                            selectConditions[field] = select[field];
                        }
                    }

                    // 如果一个有效field都没有被包含！没有的话就报错！
                    if (Object.keys(selectConditions).length === 0) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you used invalid parameters for select query. " +
                                "Notice, select query only takes '_id', 'name', 'email', 'pendingTasks', and 'dataCreated' as parameters, and the values can only be set to 1 or -1. " +
                                "Plus, you have to choose at least one field as the parameter. " +
                                "Additionally, '_id' will always be shown only if you straightforwardly set it to 0." +
                                "Note! When you set more than one filed in select query, except the '_id' filed, all other fields should be set to 0 or 1, you cannot set some of them to 1 and others to 0.",
                            data: []
                        });
                    }
                    // Mongoose 的 select 方法。
                    query = query.select(selectConditions);

                    //console.log("Query after select:", query);

                    // 捕获除了invalid fields 以外的任何错误！
                } catch (error) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, some error happened when parsing select query, please try again." +
                            "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                            "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception.",
                        data: []
                    });
                }
            }

            // 4. skip 查询。
            // 跳过一些记录。
            // 如果query里有"skip"关键词。
            // 可以是0，如果是0的话Mongoose会返回所有记录。
            if (req.query.skip) {
                // 尝试parse出skip=后面的东西，并且尝试转化为十进制的数字。如果转换失败，就会返回特殊值NaN 。
                const skip = parseInt(req.query.skip, 10);
                // 如果skip 是NaN 或者 是负数，抛出错误。
                if (isNaN(skip) || skip < 0) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, you used an invalid value for skip query. " +
                            "Notice, skip value should be a non-negative integer. " +
                            "Plus, skip value can be 0, in this way, no records will be skipped.",
                        data: []
                    });
                }
                query = query.skip(skip);
            }

            // 5. limit 查询。
            // 限制对多返回的记录的最高个数。
            // 如果query里有"limit"关键词。
            // 可以是0，如果是0的话Mongoose会返回所有记录。
            if (req.query.limit) {
                // 尝试parse出limit=后面的东西，并且尝试转化为十进制的数字。如果转换失败，就会返回特殊值NaN 。
                const limit = parseInt(req.query.limit, 10);
                // 如果limit 是NaN 或者 是负数，抛出错误。
                if (isNaN(limit) || limit < 0) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, you used an invalid value for limit query. " +
                            "Notice, limit value should be a non-negative integer. " +
                            "Plus, limit value can be 0, in this way, there will be no limit.",
                        data: []
                    });
                }
                query = query.limit(limit);
            }

            // 6. count 查询。
            // 允许对所有query进行count。
            // 如果有count查询，
            if (req.query.count === "true") {
                // Mongoose使用countDocuments方法，计数查询query（已经满足前面所有的query条件）中的document数量。
                const count = await query.countDocuments();
                // 200。返回的是count。
                return res.status(200).json({
                    message: "200 Success -> This is the count of documents according to the query.",
                    data: count
                });
            } else {
                // 如果用户没有要求count 查询。返回正常数据。
                // 先等待 所有query 的搜索结果执行！并把它赋值给 新的 users 变量。
                const users = await query.exec();
                //console.log("Query Result:", users);
                // 如果对 /api/users/XXXX={...} 使用 GET 方法，则返回 users data （此时已经包括了query的内容）! 200成功。
                // XXXX={...}是可有可无的，如果没有添加任何，那么就只返回最初的query=User.find()，即所有符合User model的结果。
                res.status(200).json({
                    // 设定message 。
                    message: "200 Success -> Users are retrieved successfully! Here is the list of users in database!",
                    // 返回的 data 为 users !
                    data: users
                });
            }

            // 任何的错误！500错误。
        } catch (error) {
            //console.error("Error in GET /users:", error);
            res.status(500).json({
                message: "500 Server Error -> Sorry, cannot get the satisfied information! Please try again." +
                    "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                    "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception.",
                data: []
            });
        }

    });

    // POST请求，创建一个新用户并返回详细信息。
    // async function 。
    userRoute.post(async function (req, res) {
        try {
            //console.log("Received POST request on /users with body:", req.body);
            // req.body 是 用户输入的 JSON 信息。
            // 解构并取出 req.body 里的 name 和 email 和pendingTasks 和dateCreated 。
            const { name, email, pendingTasks, dateCreated } = req.body;

            // 确保name和email都已提供！如果没有，报错！
            if (!name || !email) {
                return res.status(400).json({
                    message: "400 Error -> Sorry, both the name and the email of the user are required!",
                    data: []
                });
            }

            // 检查邮箱是否 unique ！用 User.findOne 的模板排查。
            const existingUser = await User.findOne({ email });
            // 如果已经被用过，就要报错。
            if (existingUser) {
                return res.status(400).json({
                    message: "400 Error -> Sorry, this user's email is already in use!",
                    data: []
                });
            }

            // 创建一个新用户。用 User model，input是必须有的 name 和 emai。
            const newUser = new User({
                name,
                email,
                pendingTasks: [],
                // 如果req.body没有提供的话，就会等于默认值。
                dateCreated
            });

            // 开始判断post user 的时候，有没有再pendingTasks里写内容！
            // 先判断是不是合法的list ， 看看是不是array ， 看看是不是每一项都是task id ！
            if (Array.isArray(pendingTasks)) {
                //console.log("yes")
                //console.log(pendingTasks)
                // 对于每个pendingTasks 里的任务。
                for (const taskId of pendingTasks) {
                    //console.log(typeof taskId)
                    if (!ObjectId.isValid(taskId)) {
                        //console.log("yes1")
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you entered invalid type of inputs pendingTasks! The inputs must be the object id of task!",
                            data: []
                        });
                    }
                    //console.log("here")
                    // 检查task 存不存在！如果不存在，报错。
                    let task = await Task.findById(taskId);
                    //console.log(task)
                    if (!task) {
                        //console.log("yes2")
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you put an id of a task in pendingTasks which does not exist!",
                            data: []
                        });
                    }
                    // 检查task 是否被完成！
                    if (task.completed) {
                        //console.log("yes3")
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you put a task which is already completed in pendingTasks! " +
                            "You can only put not completed tasks!",
                            data: []
                        });
                    }
                    // 检查task 是否已经有assignedUser！
                    if (task.assignedUser) {
                        //console.log("yes3")
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you put a task which already has an assignedUser! " +
                            "You can only put a task whose assignedUser is null!",
                            data: []
                        });
                    }
                    // 更新 task 的 user id ！
                    task.assignedUser = newUser._id;
                    // 更新 task 的 user name !
                    task.assignedUserName = newUser.name;
                    //console.log(task)
                    // 保存task 的信息！
                    await task.save();
                    // 一切合法，成功push !
                    if (!newUser.pendingTasks.includes(task._id)) {
                        newUser.pendingTasks.push(task._id);
                    }
                    //console.log(newUser)
                }
            }

            // 这个时候再save！
            await newUser.save();
            //console.log("New User Created:", newUser);
            // 表示创建成功。201成功。
            res.status(201).json({
                message: "201 Creation Success -> This user has been created successfully!",
                data: newUser
            });

            // 如果有错误，报错。500错误。
        } catch (error) {
            //console.error("Error in POST /users:", error);
            res.status(500).json({
                message: "500 Server Error -> Sorry, cannot post this user! Please try again. " +
                    "A kind reminder: you should put valid input for pendingTasks or leave it as empty. " +
                    "A valid input of pendingTasks is a series of id of the task which exists, has not been completed yet, and has no assigned user.",
                data: []
            });
        }
    });

    return router;
};

