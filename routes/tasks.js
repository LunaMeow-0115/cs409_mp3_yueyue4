// 引入Task模型
var Task = require('../models/task');
// 引入User模型。
var User = require('../models/user');

module.exports = function(router) {

    // GET 请求：获取任务列表。
    router.get('/tasks', async (req, res) => {
        try {

            let finalResponse = Task.find();

            // 1. where 查询。
            if (req.query.where) {
                try {
                    const where = JSON.parse(req.query.where);
                    const validWhereFields = ['_id', 'name', 'description', 'deadline', 'completed', 'assignedUser', 'assignedUserName', 'dateCreated'];
                    let whereConditions = {};
                    for (let field in where) {
                        if (validWhereFields.includes(field)) {
                            whereConditions[field] = where[field];
                        }
                    }
                    if (Object.keys(whereConditions).length === 0) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you used invalid or null parameters for where query. " +
                                "Notice，where query only takes '_id', 'name', 'description', 'deadline', 'completed', 'assignedUser', 'assignedUserName', and 'dateCreated' as parameters. " +
                                "Plus, you should choose at least one field as the parameter.",
                            data: []
                        });
                    }
                    finalResponse = finalResponse.where(where);
                } catch (error) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, some error happened when parsing where query, please try again.",
                        data: []
                    });

                }
            }

            // 2. sort 查询。
            if (req.query.sort) {
                try {
                    const sort = JSON.parse(req.query.sort);
                    const validSortFields = ['_id', 'name', 'description', 'deadline', 'completed', 'assignedUser', 'assignedUserName', 'dateCreated'];
                    const sortConditions = {};
                    for (let field in sort) {
                        if (validSortFields.includes(field) && [1, -1].includes(sort[field])) {
                            sortConditions[field] = sort[field];
                        }
                    }
                    if (Object.keys(sortConditions).length === 0) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you used invalid parameters for sort query. " +
                                "Notice, sort query only takes '_id', 'name', 'description', 'deadline', 'completed', 'assignedUser', 'assignedUserName', and 'dateCreated' as parameters, and the values can only be set to 1 or -1. " +
                                "You can set sort query as null, in this way, you will get the original list of users without any sorting.",
                            data: []
                        });
                    }
                    finalResponse = finalResponse.sort(sort);
                } catch (error) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, some error happened when parsing sort query, please try again.",
                        data: []
                    });
                }
            }

            // 3. select 查询。
            if (req.query.select) {
                try {
                    const select = JSON.parse(req.query.select);
                    const validSelectFields = ['_id', 'name', 'description', 'deadline', 'completed', 'assignedUser', 'assignedUserName', 'dateCreated'];
                    let selectConditions = {};
                    for (let field in select) {
                        if (validSelectFields.includes(field) && [0, 1].includes(select[field])) {
                            selectConditions[field] = select[field];
                        }
                    }

                    // 如果一个有效field都没有被包含！没有的话就报错！
                    if (Object.keys(selectConditions).length === 0) {
                        return res.status(400).json({
                            message: "400 Error -> Sorry, you used invalid parameters for select query. " +
                                "Notice, select query only takes '_id', 'name', 'description', 'deadline', 'completed', 'assignedUser', 'assignedUserName', and 'dateCreated' as parameters, and the values can only be set to 1 or 0. " +
                                "Plus, you have to choose at least one field as the parameter. " +
                                "Additionally, '_id' will always be shown only if you straightforwardly set it to 0." +
                                "Note! When you set more than one filed in select query, except the '_id' filed, all other fields should be set to 0 or 1, you cannot set some of them to 1 and others to 0.",
                            data: []
                        });
                    }
                    // Mongoose 的 select 方法。
                    finalResponse = finalResponse.select(selectConditions);
                } catch (error) {
                    //console.log(error)
                    return res.status(400).json({
                        message: "400 Error -> Sorry, some error happened when parsing select query, please try again." +
                            "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                            "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception.",
                        data: []
                    });
                }
            }

            // 4. skip 查询。
            if (req.query.skip) {
                const skip = parseInt(req.query.skip, 10);
                if (isNaN(skip) || skip < 0) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, you used an invalid value for skip query. " +
                            "Notice, skip value should be a non-negative integer. " +
                            "Plus, skip value can be 0, in this way, no records will be skipped.",
                        data: []
                    });
                }
                finalResponse = finalResponse.skip(skip);
            }

            // 5. limit 查询。
            if (req.query.limit) {
                const limit = parseInt(req.query.limit, 10);
                // 如果limit 是NaN 或者 是负数，抛出错误。
                if (isNaN(limit) || limit < 0) {
                    return res.status(400).json({
                        message: "400 Error -> Sorry, you used an invalid value for limit query. " +
                            "Notice, limit value should be a non-negative integer. " +
                            "Plus, limit value can be 0, in this way, there will be no limit.",
                        data: []
                    });
                }
                finalResponse = finalResponse.limit(limit);
            }

            // 6. count 查询。
            if (req.query.count === "true") {
                const count = await finalResponse.countDocuments();
                return res.status(200).json({
                    message: "200 Success -> This is the count of documents according to the query.",
                    data: count
                });
            } else {
                const tasks = await finalResponse.exec();
                res.status(200).json({
                    message: "200 Success -> Tasks are retrieved successfully! Here is the list of users in database!",
                    data: tasks
                });
            }

        } catch (error) {
            res.status(500).json({
                message: "500 Server Error -> Sorry, cannot get the satisfied information! Please try again." +
                    "A kind reminder: If you used select query and chose more than one field, except the '_id' filed, please set all the fields to 0 or 1, you cannot set some fields to 1 and other to 0." +
                    "The '_id' field will always be shown only if you straightforwardly set it to 0, so it is an exception.",
                data: []
            });
        }
    });

    // POST 请求：创建新任务。
    router.post('/tasks', async (req, res) => {
        try {
            // 结构POST的JSON body 。completed如果没有写的话就默认为false 。只解构出来后期需要用到的fields 。
            let { name, deadline, assignedUser, completed } = req.body;
            // 确保task name和deadline 都被提供。
            if (!name || !deadline) {
                return res.status(400).json({
                    message: "400 Error -> Sorry, both the name and the deadline of the task are required!",
                    data: []
                });
            }

            // 先设定user name是unassigned 。
            let assignedUserName = "unassigned";
            let user;
            // 如果有设定assignedUser 。
            // 检查是否是null !!!! 满足dataFill.py 的要求，因为这个脚本允许assignedUser="" ！
            if (assignedUser && assignedUser.trim() !== "") {
                // 查看user 存在不存在！不存在就报错。
                user = await User.findById(assignedUser);
                if (!user) {
                    return res.status(400).json({
                        message: "400 Error -> The user you want to assign the task with does not exist.",
                        data: []
                    });
                }
                // 如果存在，就把user name设定好。
                assignedUserName = user.name;
            } else {
                 //如果没有指定 assignedUser 或 assignedUser 为空字符串，将其设置为 null
                assignedUser = null;
            }

            // 创建新任务。
            const newTask = new Task({ name, deadline, assignedUser, assignedUserName, completed });
            await newTask.save();

            // 如果指定了用户且任务未完成，再将任务添加到用户的pendingTasks中！
            // 当 completed 为 false（任务未完成），!completed 将为 true，表示该任务应该被加入到用户的 pendingTasks 数组中。
            // 当 completed 为 true（任务已完成），!completed 将为 false，表示不需要把任务加入到 pendingTasks 中。
            if (user && !completed) {
                user.pendingTasks.push(newTask._id);
                await user.save();
            }

            // 返回创建成功的提示。
            res.status(201).json({
                message: "201 Creation Success -> A new task has been successfully created!",
                data: newTask
            });

        } catch (error) {
            //console.log(error)
            res.status(500).json({
                message: "500 Server Error -> Sorry, cannot get the satisfied information! Please try again.",
                data: []
            });
        }
    });

    return router;
};

