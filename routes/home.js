// 导入secrets的connection string。
var secrets = require('../config/secrets');

// 这段代码导出了一个函数，这个函数接收一个 router 参数。这代表可以在主应用文件中导入 home.js，并传递一个 Express 路由器对象给它。
module.exports = function (router) {

    // 对于这个根路径的 GET 请求，设置了一个回调函数，当这个路由被访问时，会执行这个函数。
    var homeRoute = router.route('/');

    // 对应 index.js 中， 因为写了app.use('/api', require('./home.js')(router));
    // 所以，该function会响应对 /api 的GET请求。
    homeRoute.get(function (req, res) {

        // 在回调函数内部，从先前导入的 secrets 模块中获取名为 token 的属性值，并将其赋值给 connectionString 变量。
        var connectionString = secrets.token;

        // 回调函数使用 res.json() 发送一个 JSON 响应，包含一条message显示token。
        res.json({ message: 'My connection string is ' + connectionString });
    });

    // 函数返回陪之后的"router"对象。
    return router;
}

